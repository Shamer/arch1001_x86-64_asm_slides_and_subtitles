1
00:00:00,080 --> 00:00:03,120
now there are actually certain times

2
00:00:01,599 --> 00:00:05,839
when you as a programmer may

3
00:00:03,120 --> 00:00:07,520
have to write something in assembly this

4
00:00:05,839 --> 00:00:09,280
is pretty common if you're writing some

5
00:00:07,520 --> 00:00:12,000
low-level code like firmware

6
00:00:09,280 --> 00:00:13,920
operating systems or drivers because

7
00:00:12,000 --> 00:00:14,480
frequently you'll be called upon to

8
00:00:13,920 --> 00:00:16,240
interact

9
00:00:14,480 --> 00:00:18,720
directly with the hardware and there

10
00:00:16,240 --> 00:00:20,480
won't be any C mechanism for interacting

11
00:00:18,720 --> 00:00:21,600
with your specific hardware for your

12
00:00:20,480 --> 00:00:23,680
specific architecture

13
00:00:21,600 --> 00:00:26,160
obviously there's nothing in C that says

14
00:00:23,680 --> 00:00:28,240
how to interact with a control register

15
00:00:26,160 --> 00:00:30,400
on a Intel platform versus an arm

16
00:00:28,240 --> 00:00:30,880
platform versus a mips platform or for

17
00:00:30,400 --> 00:00:32,880
instance

18
00:00:30,880 --> 00:00:35,360
if you were writing a cryptographic

19
00:00:32,880 --> 00:00:36,800
library then you might want access to

20
00:00:35,360 --> 00:00:38,960
some accelerated

21
00:00:36,800 --> 00:00:40,480
crypto assembly instructions that the

22
00:00:38,960 --> 00:00:41,040
particular architecture might make

23
00:00:40,480 --> 00:00:43,840
available

24
00:00:41,040 --> 00:00:45,200
Intel has such as instructions as do

25
00:00:43,840 --> 00:00:46,960
other platforms

26
00:00:45,200 --> 00:00:49,360
or maybe you're just trying to really

27
00:00:46,960 --> 00:00:50,000
super performance optimize some code and

28
00:00:49,360 --> 00:00:51,920
you want

29
00:00:50,000 --> 00:00:53,840
full control and you want to be able to

30
00:00:51,920 --> 00:00:55,840
run some tests and find out what the

31
00:00:53,840 --> 00:00:56,160
fastest way to run a particular function

32
00:00:55,840 --> 00:00:57,600
is

33
00:00:56,160 --> 00:00:59,039
these are all reasons why you might go

34
00:00:57,600 --> 00:00:59,680
down to the assembly level for the

35
00:00:59,039 --> 00:01:01,600
purposes

36
00:00:59,680 --> 00:01:03,520
of this class and for helping you

37
00:01:01,600 --> 00:01:04,320
understand assembly and being able to

38
00:01:03,520 --> 00:01:06,479
experiment

39
00:01:04,320 --> 00:01:08,960
and learning to fish for yourself for

40
00:01:06,479 --> 00:01:10,960
the purposes of this class we like being

41
00:01:08,960 --> 00:01:12,720
able to write raw assembly just because

42
00:01:10,960 --> 00:01:14,880
it helps you experiment

43
00:01:12,720 --> 00:01:16,400
and see for instance if you write this

44
00:01:14,880 --> 00:01:17,840
assembly instruction you can step

45
00:01:16,400 --> 00:01:20,000
through the code and see how

46
00:01:17,840 --> 00:01:21,680
the see what the net results of that

47
00:01:20,000 --> 00:01:22,400
assembly instruction were and just as a

48
00:01:21,680 --> 00:01:24,240
warning though

49
00:01:22,400 --> 00:01:26,400
different assemblers support different

50
00:01:24,240 --> 00:01:28,080
syntaxes that are all slightly different

51
00:01:26,400 --> 00:01:30,159
than what you might actually see

52
00:01:28,080 --> 00:01:32,000
when you're disassembling something

53
00:01:30,159 --> 00:01:32,960
consequently you'll frequently find

54
00:01:32,000 --> 00:01:35,680
yourself having to

55
00:01:32,960 --> 00:01:37,520
search for how to correct how to write a

56
00:01:35,680 --> 00:01:38,880
particular assembly instruction or how

57
00:01:37,520 --> 00:01:40,000
to write a particular sequence of

58
00:01:38,880 --> 00:01:41,680
instructions

59
00:01:40,000 --> 00:01:43,520
in the particular assembler now there's

60
00:01:41,680 --> 00:01:44,320
generally two ways that you could write

61
00:01:43,520 --> 00:01:46,399
assembly there's

62
00:01:44,320 --> 00:01:47,920
inline assembly and there's out-of-band

63
00:01:46,399 --> 00:01:49,600
assembly inline

64
00:01:47,920 --> 00:01:51,840
is where you actually just place the

65
00:01:49,600 --> 00:01:54,799
assembly directly in line with some C

66
00:01:51,840 --> 00:01:56,320
or C++ code gcc supports this if

67
00:01:54,799 --> 00:01:59,280
you use inline assembly

68
00:01:56,320 --> 00:02:01,360
in GNU assembler or gas syntax and

69
00:01:59,280 --> 00:02:03,439
visual studio used to support inline

70
00:02:01,360 --> 00:02:05,280
assembly for 32-bit code

71
00:02:03,439 --> 00:02:06,479
but it doesn't support it anymore for

72
00:02:05,280 --> 00:02:08,399
64-bit code

73
00:02:06,479 --> 00:02:10,239
so if you were writing 32-bit code you

74
00:02:08,399 --> 00:02:12,080
could still use inline assembly

75
00:02:10,239 --> 00:02:13,920
but for the purposes of this class since

76
00:02:12,080 --> 00:02:15,440
we want to focus on 64-bit

77
00:02:13,920 --> 00:02:17,280
we're not going to cover that so

78
00:02:15,440 --> 00:02:19,440
standalone assembly on the other hand is

79
00:02:17,280 --> 00:02:21,520
where you put all of your assembly into

80
00:02:19,440 --> 00:02:23,680
a standalone a separate file not

81
00:02:21,520 --> 00:02:24,640
inside of the main C file that you're

82
00:02:23,680 --> 00:02:26,800
working on

83
00:02:24,640 --> 00:02:29,120
and then you have the assembler compile

84
00:02:26,800 --> 00:02:31,280
it and then that compiled object file

85
00:02:29,120 --> 00:02:33,040
gets linked in with the main binary this

86
00:02:31,280 --> 00:02:35,280
is the path we actually have to use for

87
00:02:33,040 --> 00:02:36,080
visual studio if we want to write 64-bit

88
00:02:35,280 --> 00:02:38,000
assembly

89
00:02:36,080 --> 00:02:39,920
very frequently beyond the fact that you

90
00:02:38,000 --> 00:02:41,200
know a given assembler might write

91
00:02:39,920 --> 00:02:42,720
something slightly different

92
00:02:41,200 --> 00:02:44,879
there's a lot of different helper

93
00:02:42,720 --> 00:02:48,080
mechanisms that the assembly will

94
00:02:44,879 --> 00:02:49,680
assembler will build in things to allow

95
00:02:48,080 --> 00:02:51,120
someone who has to write all of their

96
00:02:49,680 --> 00:02:53,760
code in assembly

97
00:02:51,120 --> 00:02:55,200
to have you know easier ways to access

98
00:02:53,760 --> 00:02:59,280
you know global memory

99
00:02:55,200 --> 00:03:02,000
or easier ways to call into functions

100
00:02:59,280 --> 00:03:03,680
so masm is the thing that visual studio

101
00:03:02,000 --> 00:03:04,560
is actually going to use the microsoft

102
00:03:03,680 --> 00:03:06,879
assembler

103
00:03:04,560 --> 00:03:08,159
nasm is a very popular alternative

104
00:03:06,879 --> 00:03:10,879
network assembler

105
00:03:08,159 --> 00:03:12,080
i think frequently i would use yasm back

106
00:03:10,879 --> 00:03:14,080
in the day which was

107
00:03:12,080 --> 00:03:15,280
yet another assembler the key benefit of

108
00:03:14,080 --> 00:03:17,599
something like nasm

109
00:03:15,280 --> 00:03:19,200
is that it is cross-platform which means

110
00:03:17,599 --> 00:03:21,599
that if you wrote something

111
00:03:19,200 --> 00:03:23,440
in nasm you could potentially compile it

112
00:03:21,599 --> 00:03:25,599
on Windows or Linux

113
00:03:23,440 --> 00:03:27,519
another very good capability that you

114
00:03:25,599 --> 00:03:30,159
might want to make use of at some point

115
00:03:27,519 --> 00:03:32,959
is the capability to emit raw bytes

116
00:03:30,159 --> 00:03:34,879
into an assembly stream and so this

117
00:03:32,959 --> 00:03:37,480
basically says instead of writing a

118
00:03:34,879 --> 00:03:40,239
human readable form of you know add

119
00:03:37,480 --> 00:03:42,000
rax, 1 you will write you know some

120
00:03:40,239 --> 00:03:44,799
specific bytes that should

121
00:03:42,000 --> 00:03:45,599
turn into and be interpreted by the

122
00:03:44,799 --> 00:03:48,560
processor

123
00:03:45,599 --> 00:03:50,319
as add rax, 1 this is a great way for you

124
00:03:48,560 --> 00:03:51,040
to actually check your understanding of

125
00:03:50,319 --> 00:03:53,439
the manual

126
00:03:51,040 --> 00:03:54,799
so after you're able to read the manual

127
00:03:53,439 --> 00:03:56,239
you can now see like here are the

128
00:03:54,799 --> 00:03:58,480
different ways that you can write

129
00:03:56,239 --> 00:03:59,360
bytes and create particular assembly

130
00:03:58,480 --> 00:04:01,040
instructions

131
00:03:59,360 --> 00:04:02,799
so this will be one of the labs we'll do

132
00:04:01,040 --> 00:04:04,159
in a little bit is just writing bytes

133
00:04:02,799 --> 00:04:05,519
and seeing whether or not you generate

134
00:04:04,159 --> 00:04:06,400
the right assembly instruction that you

135
00:04:05,519 --> 00:04:07,519
expected

136
00:04:06,400 --> 00:04:09,360
and that'll tell you whether or not

137
00:04:07,519 --> 00:04:09,920
you're interpreting the Intel manual

138
00:04:09,360 --> 00:04:12,799
correctly

139
00:04:09,920 --> 00:04:13,360
very occasionally this is also useful

140
00:04:12,799 --> 00:04:15,599
for

141
00:04:13,360 --> 00:04:16,479
if you can't find the right syntax from

142
00:04:15,599 --> 00:04:18,639
a particular

143
00:04:16,479 --> 00:04:20,079
assembler and you just you know you want

144
00:04:18,639 --> 00:04:22,000
to stop fighting you want to stop

145
00:04:20,079 --> 00:04:23,600
searching around for the right syntax

146
00:04:22,000 --> 00:04:24,479
and you just say look I know what I'm

147
00:04:23,600 --> 00:04:26,720
trying to do

148
00:04:24,479 --> 00:04:28,400
I know what the Intel manual says the

149
00:04:26,720 --> 00:04:29,360
right bytes are to create this assembly

150
00:04:28,400 --> 00:04:31,280
instruction

151
00:04:29,360 --> 00:04:33,199
and so you just create it manually by

152
00:04:31,280 --> 00:04:36,240
spitting out the bytes and the CPU is

153
00:04:33,199 --> 00:04:36,240
just fine with that

