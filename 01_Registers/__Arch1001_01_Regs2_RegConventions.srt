0
00:00:00,080 --> 00:00:04,319
now intel had some register usage

1
00:00:02,480 --> 00:00:05,440
conventions that they suggested in their

2
00:00:04,319 --> 00:00:08,160
manuals

3
00:00:05,440 --> 00:00:09,280
and frequently these had to do with like

4
00:00:08,160 --> 00:00:11,920
the naming actually

5
00:00:09,280 --> 00:00:13,679
and had to do with the usage but in this

6
00:00:11,920 --> 00:00:15,360
class we're not going to necessarily see

7
00:00:13,679 --> 00:00:16,720
all of these recommendations so

8
00:00:15,360 --> 00:00:18,160
i've labeled in green the ones that

9
00:00:16,720 --> 00:00:18,720
we're specifically going to see in the

10
00:00:18,160 --> 00:00:20,400
class

11
00:00:18,720 --> 00:00:22,560
and red as things we're not going to

12
00:00:20,400 --> 00:00:24,640
see in this class so for instance

13
00:00:22,560 --> 00:00:25,760
rax in this class we will see it

14
00:00:24,640 --> 00:00:28,320
storing

15
00:00:25,760 --> 00:00:29,599
function return values also you know

16
00:00:28,320 --> 00:00:31,119
what we're not going to see in the class

17
00:00:29,599 --> 00:00:34,320
is that intel originally

18
00:00:31,119 --> 00:00:36,239
suggested that a be the accumulator

19
00:00:34,320 --> 00:00:38,160
and that you the assembly would just

20
00:00:36,239 --> 00:00:40,079
accumulate so if you do you know

21
00:00:38,160 --> 00:00:41,280
one plus two the result should go back

22
00:00:40,079 --> 00:00:43,680
into a

23
00:00:41,280 --> 00:00:44,399
register the ax register the rax

24
00:00:43,680 --> 00:00:45,600
register

25
00:00:44,399 --> 00:00:47,120
but we're not actually going to see that

26
00:00:45,600 --> 00:00:49,680
that much compilers don't tend to

27
00:00:47,120 --> 00:00:52,160
necessarily do that that much anymore

28
00:00:49,680 --> 00:00:53,840
so rbx base pointer to the data section

29
00:00:52,160 --> 00:00:55,280
i have seen this before but

30
00:00:53,840 --> 00:00:56,840
our assembly is going to be so simple

31
00:00:55,280 --> 00:00:58,879
here that we're not really going to see

32
00:00:56,840 --> 00:01:01,280
that rcx

33
00:00:58,879 --> 00:01:03,359
c for counter is frequently used for

34
00:01:01,280 --> 00:01:05,840
looping operations

35
00:01:03,359 --> 00:01:07,680
and so c for counter rdx we're not

36
00:01:05,840 --> 00:01:09,520
going to see it used as an I/O pointer

37
00:01:07,680 --> 00:01:10,640
in this class but we will in a future

38
00:01:09,520 --> 00:01:14,159
architecture class

39
00:01:10,640 --> 00:01:16,080
then for rsi source index pointer for

40
00:01:14,159 --> 00:01:17,119
string operations we'll see towards the

41
00:01:16,080 --> 00:01:18,960
end of the class

42
00:01:17,119 --> 00:01:20,400
a looping string operation thing where

43
00:01:18,960 --> 00:01:23,759
rsi will have a special

44
00:01:20,400 --> 00:01:25,119
usage rdi is destination index so source

45
00:01:23,759 --> 00:01:27,119
to destination

46
00:01:25,119 --> 00:01:28,479
but again those are just sort of naming

47
00:01:27,119 --> 00:01:31,280
and recommendations

48
00:01:28,479 --> 00:01:33,280
they can be just used as any old general

49
00:01:31,280 --> 00:01:36,400
purpose register if the compiler wants

50
00:01:33,280 --> 00:01:38,079
rsp sp for stack pointer points at the

51
00:01:36,400 --> 00:01:39,439
top of the stack meaning the last value

52
00:01:38,079 --> 00:01:42,720
that was put on the stack

53
00:01:39,439 --> 00:01:43,439
rbp stack frame base pointer it's used

54
00:01:42,720 --> 00:01:45,759
to point at

55
00:01:43,439 --> 00:01:47,119
the base of the current stack frame and

56
00:01:45,759 --> 00:01:50,320
talk about stack next

57
00:01:47,119 --> 00:01:52,159
and rip it points at the next

58
00:01:50,320 --> 00:01:53,600
instruction the instruction pointer

59
00:01:52,159 --> 00:01:54,720
points at the next instruction that the

60
00:01:53,600 --> 00:01:57,119
processor should

61
00:01:54,720 --> 00:01:57,119
execute

